var redis = require('redis');
var request = require('superagent');
var wowuction = require('./wowuction');
var redisPort = process.env.OPENSHIFT_REDIS_PORT||6379;
var redisHost = process.env.OPENSHIFT_REDIS_HOST||'127.0.0.1';
var redisPassword = process.env.REDIS_PASSWORD||'';
var client = redis.createClient(redisPort,redisHost);
client.auth(redisPassword, function(err){
  if(err){
    console.log("Redis login failed. Terminating.")
    process.exit(1);
  }
});
var itemInfoHelper = function(data,item,cb){
  var itemData = {};
  if(data.byId[item]){
    itemData = data.byId[item];
    return cb(null,itemData);
  }
  return cb({"error":"Something went wrong"},null);
};

var getItemInfo = function(realm,item,cb){
  var rlm = realm.region+'-'+realm.name;
  client.get(rlm,function(err,reply){
    if(reply === null || err){
      var url = 'http://www.wowuction.com/'+
        realm.region.toLowerCase()+
        '/'+realm.name.toLowerCase()+
        '/horde/Tools/RealmDataExportGetFileStatic?type=csv&token='+process.env.WOWUCTION_TOKEN;
      request.get(url)
      .end(function(error,res){
        if(res.statusCode === 200 && !error){
          var data = wowuction(res.text);
          client.set(rlm,JSON.stringify(data),function(){
            client.expire(rlm,60*60);
          });
          return itemInfoHelper(data,item.id,cb);
        }else{
          return cb({"error":"error fetching wowuctiondata"},null);
        }
      });
    }else{
      return itemInfoHelper(JSON.parse(reply),item.id,cb);
    }
  });
};
module.exports = {
  getItemInfo: getItemInfo
};
