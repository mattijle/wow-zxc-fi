var _ = require('lodash');
module.exports = function(data){
  var wowuction = {};
  data = data.split('\n');
  var fieldNames = _.slice(data,0,1)[0].split(',');
  var ahRows = _.slice(data,1,data.length);
  wowuction.byId = wowuction.byId || {};
  wowuction.byName = wowuction.byName || {};
  ahRows.forEach(function(e){
    var row = e.split(',');
    var zip = _.zipObject(fieldNames,row);
    var id = zip['Item ID'];
    var name = zip['Item Name'];
    wowuction.byId[id] = zip;
    wowuction.byName[name] = zip;
  });
  return wowuction;
};
