###wow.zxc.fi
We provide simple json api to Wowuction data

Usage:
```
curl -H "Content-Type: application/json" -X POST -d '{"realm":"Daggerspine","region":"US","items":[{"id":"113588","fields":["AH MarketPrice"]}]}' http://wow.zxc.fi/item.json
```

Result:
```
{"realm":"Daggerspine","region":"US","items":[{"id":"113588","fields":[{"name":"AH MarketPrice","value":"167"}]}]}
```
List of fields you can request:
===
```
Realm Name
Export Time
PMktPrice Date
Reserved
Item ID
Item Name
AH MarketPrice Coppers
AH Quantity
AH MarketPrice
AH MinimumPrice
14-day Median Market Price
Median Market Price StdDev
14-day Todays PMktPrice
PMktPrice StdDev
Daily Price Change
Avg Daily Posted
Avg Estimated Daily Sold
Estimated Demand
```
